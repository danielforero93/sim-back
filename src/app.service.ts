import { Injectable, Inject } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ConfigType } from '@nestjs/config';
import config from './config';

@Injectable()
export class AppService {
  constructor(
    @Inject(config.KEY) private configServiceTipado: ConfigType<typeof config>,
    private configService: ConfigService,
    @Inject('API_KEY') private apiKey: string,
    @Inject('TASKS_EXAMPLE') private tasksExampleList: any[],
  ) {}

  getHello(): string {
    // const apiKeyOld = this.configService.get('API_KEY');
    // const databaseNameOld = this.configService.get('DATABASE_NAME');

    const apiKey = this.configServiceTipado.apiKey;
    const databaseName = this.configServiceTipado.database.name;

    return `Hello world:  API_KEY definida en el archivo .env es: ${apiKey}, Y DATABASE_NAME es: ${databaseName}`;
  }

  getApiKey(): string {
    console.log(this.tasksExampleList);
    // return 'La api key es  : ' + this.apiKey;
    return 'La api key es  : ' + this.configServiceTipado.apiKey;
  }

  // getTasks(): string {
  //   return ``;
  // }
}
