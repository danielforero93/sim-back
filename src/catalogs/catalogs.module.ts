import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CatalogsController } from './catalogs.controller';
import { CatalogsService } from './catalogs.service';

import { ModulesSimController } from './controllers/modules-sim/modules-sim.controller';
import { ModulesSimService } from './services/modules-sim/modules-sim.service';
import { ModuleSim } from './entities/module-sim.entity';

import { ActivitiesModuleController } from './controllers/activities-module/activities-module.controller';
import { ActivitiesModuleService } from './services/activities-module/activities-module.service';
import { ActivityModule } from './entities/activity-module.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ModuleSim, ActivityModule])],
  controllers: [
    CatalogsController,
    ModulesSimController,
    ActivitiesModuleController,
  ],
  providers: [CatalogsService, ModulesSimService, ActivitiesModuleService],
})
export class CatalogsModule {}
