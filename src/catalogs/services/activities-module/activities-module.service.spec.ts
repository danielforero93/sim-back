import { Test, TestingModule } from '@nestjs/testing';
import { ActivitiesModuleService } from './activities-module.service';

describe('ActivitiesModuleService', () => {
  let service: ActivitiesModuleService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ActivitiesModuleService],
    }).compile();

    service = module.get<ActivitiesModuleService>(ActivitiesModuleService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
