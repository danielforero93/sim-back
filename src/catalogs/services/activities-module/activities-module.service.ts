import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import {
  CreateActivityModuleDto,
  UpdateActivityModuleDto,
} from 'src/catalogs/dto/activity-module.dto';
import { ActivityModule } from 'src/catalogs/entities/activity-module.entity';
import { ModulesSimService } from '../modules-sim/modules-sim.service';

@Injectable()
export class ActivitiesModuleService {
  constructor(
    @InjectRepository(ActivityModule)
    private activityModuleRepo: Repository<ActivityModule>,
    private modulesSimService: ModulesSimService,
  ) {}

  findAll() {
    return this.activityModuleRepo.find();
  }

  async findOne(id: number) {
    const activityModule = await this.activityModuleRepo.findOne({
      where: { id: id },
      relations: ['moduleSim'],
    });
    console.log(activityModule);
    if (!activityModule)
      throw new NotFoundException(`Avtividad de Modulo  # ${id} no encontrada`);
    return activityModule;
  }

  async create(data: CreateActivityModuleDto) {
    const newActivityModule = this.activityModuleRepo.create(data);
    if (data.moduleSimId) {
      const moduleSim = await this.modulesSimService.findOne(data.moduleSimId);
      newActivityModule.moduleSim = moduleSim;
    }

    return this.activityModuleRepo.save(newActivityModule);
  }

  async update(id: number, changes: UpdateActivityModuleDto) {
    const activityModule = await this.activityModuleRepo.findOneBy({ id });
    if (changes.moduleSimId) {
      const moduleSim = await this.modulesSimService.findOne(
        changes.moduleSimId,
      );
      activityModule.moduleSim = moduleSim;
    }
    this.activityModuleRepo.merge(activityModule, changes);
    return this.activityModuleRepo.save(activityModule);
  }

  remove(id: number) {
    return this.activityModuleRepo.delete(id);
  }
}
