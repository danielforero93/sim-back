import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import {
  CreateModuleSimDto,
  UpdateModuleSimDto,
} from 'src/catalogs/dto/module-sim.dto';
import { ModuleSim } from 'src/catalogs/entities/module-sim.entity';

@Injectable()
export class ModulesSimService {
  constructor(
    @InjectRepository(ModuleSim) private moduleSimRepo: Repository<ModuleSim>,
  ) {}

  findAll() {
    return this.moduleSimRepo.find();
  }

  async findOne(id: number) {
    const moduleSim = await this.moduleSimRepo.findOne({
      where: { id: id },
      relations: ['activitiesModule'],
    });
    console.log(moduleSim);
    if (!moduleSim)
      throw new NotFoundException(`Modulo del sim # ${id} no encontrado`);
    return moduleSim;
  }

  create(data: CreateModuleSimDto) {
    const newModuleSim = this.moduleSimRepo.create(data);
    return this.moduleSimRepo.save(newModuleSim);
  }

  async update(id: number, changes: UpdateModuleSimDto) {
    const moduleSim = await this.moduleSimRepo.findOneBy({ id });
    this.moduleSimRepo.merge(moduleSim, changes);
    return this.moduleSimRepo.save(moduleSim);
  }

  remove(id: number) {
    return this.moduleSimRepo.delete(id);
  }
}
