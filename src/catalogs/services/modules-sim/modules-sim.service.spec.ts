import { Test, TestingModule } from '@nestjs/testing';
import { ModulesSimService } from './modules-sim.service';

describe('ModulesSimService', () => {
  let service: ModulesSimService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ModulesSimService],
    }).compile();

    service = module.get<ModulesSimService>(ModulesSimService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
