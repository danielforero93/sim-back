import { Test, TestingModule } from '@nestjs/testing';
import { ActivitiesModuleController } from './activities-module.controller';

describe('ActivitiesModuleController', () => {
  let controller: ActivitiesModuleController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ActivitiesModuleController],
    }).compile();

    controller = module.get<ActivitiesModuleController>(ActivitiesModuleController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
