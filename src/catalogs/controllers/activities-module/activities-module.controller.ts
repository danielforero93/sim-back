import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import {
  CreateActivityModuleDto,
  UpdateActivityModuleDto,
} from 'src/catalogs/dto/activity-module.dto';
import { ActivitiesModuleService } from 'src/catalogs/services/activities-module/activities-module.service';

@ApiTags('activities-module')
@Controller('activities-module')
export class ActivitiesModuleController {
  constructor(private activitiesModuleService: ActivitiesModuleService) {}

  @Get(':id')
  get(@Param('id', ParseIntPipe) id: number) {
    return this.activitiesModuleService.findOne(id);
  }

  @ApiOperation({ summary: 'Lista de modulos del SIM' })
  @Get()
  getAll() {
    return this.activitiesModuleService.findAll();
  }

  @UseGuards(AuthGuard('jwt'))
  @Post()
  create(@Body() payload: CreateActivityModuleDto) {
    return this.activitiesModuleService.create(payload);
  }

  @Put(':id')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() payload: UpdateActivityModuleDto,
  ) {
    return this.activitiesModuleService.update(id, payload);
  }

  @Delete(':id')
  remove(@Param('id', ParseIntPipe) id: number) {
    return this.activitiesModuleService.remove(id);
  }
}
