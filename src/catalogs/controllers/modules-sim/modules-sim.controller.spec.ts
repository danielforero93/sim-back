import { Test, TestingModule } from '@nestjs/testing';
import { ModulesSimController } from './modules-sim.controller';

describe('ModulesSimController', () => {
  let controller: ModulesSimController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ModulesSimController],
    }).compile();

    controller = module.get<ModulesSimController>(ModulesSimController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
