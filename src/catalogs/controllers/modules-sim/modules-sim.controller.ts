import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';

import {
  CreateModuleSimDto,
  UpdateModuleSimDto,
} from 'src/catalogs/dto/module-sim.dto';
import { ModulesSimService } from 'src/catalogs/services/modules-sim/modules-sim.service';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { Public } from 'src/auth/decorators/public.decorator';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { Role } from 'src/auth/models/roles.model';
import { RolesGuard } from 'src/auth/guards/roles.guard';

// @UseGuards(AuthGuard('jwt'))
@UseGuards(JwtAuthGuard, RolesGuard)
@ApiTags('modules-sim')
@Controller('modules-sim')
export class ModulesSimController {
  constructor(private modulesSimService: ModulesSimService) {}

  @Public()
  @Get(':id')
  get(@Param('id', ParseIntPipe) id: number) {
    return this.modulesSimService.findOne(id);
  }

  @Public()
  @ApiOperation({ summary: 'Lista de modulos del SIM' })
  @Get()
  getAll() {
    return this.modulesSimService.findAll();
  }
  @Roles(Role.CITIZEN /*, Role.CITIZEN*/)
  @Post()
  create(@Body() payload: CreateModuleSimDto) {
    return this.modulesSimService.create(payload);
  }

  @Put(':id')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() payload: UpdateModuleSimDto,
  ) {
    return this.modulesSimService.update(id, payload);
  }

  @Delete(':id')
  remove(@Param('id', ParseIntPipe) id: number) {
    return this.modulesSimService.remove(id);
  }
}
