import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,

  // DeleteDateColumn,
} from 'typeorm';
import { Exclude, Expose } from 'class-transformer';

import { ActivityModule } from './activity-module.entity';

@Entity()
export class ModuleSim {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({ type: 'varchar', length: 100, unique: true })
  name: string;

  @Column({ type: 'varchar', length: 255, unique: true })
  url: string;

  @Column({ type: 'varchar', length: 255 })
  image: string;

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createAt: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updateAt: Date;

  @Exclude()
  @OneToMany(() => ActivityModule, (activityModule) => activityModule.moduleSim)
  activitiesModule: ActivityModule[];

  @Expose()
  get activities() {
    if (this.activitiesModule) {
      return this.activitiesModule
        .filter((item) => !!item)
        .map((item) => ({
          ...item,
          quantity: 5,
        }));
    }
    return [];
  }

  @Expose()
  get quantityActivities() {
    if (this.activitiesModule) {
      return this.activitiesModule
        .filter((item) => !!item)
        .reduce((total, item) => {
          const totalExponencial = 10 * 1;
          return total + totalExponencial;
        }, 0);
    }
    return 0;
  }
}
