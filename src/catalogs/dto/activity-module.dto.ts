import { PartialType, ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsPositive, IsString } from 'class-validator';

export class CreateActivityModuleDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly name: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  readonly description: string;

  @IsNotEmpty()
  @IsPositive()
  @ApiProperty()
  readonly moduleSimId: number;
}

export class UpdateActivityModuleDto extends PartialType(
  CreateActivityModuleDto,
) {}
