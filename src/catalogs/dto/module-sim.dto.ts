import { PartialType, ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsPositive, IsString } from 'class-validator';

export class CreateModuleSimDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  readonly name: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: 'La url de redirección al módulo' })
  readonly url: string;

  @IsNotEmpty()
  @ApiProperty({ description: 'Url de la imagen' })
  readonly image: string;

  // @IsOptional()
  // @IsPositive()
  // @ApiProperty()
  // readonly personId: number;
}

export class UpdateModuleSimDto extends PartialType(CreateModuleSimDto) {}
