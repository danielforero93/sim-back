import { Module, Global } from '@nestjs/common';
import { lastValueFrom } from 'rxjs';
import { HttpModule, HttpService } from '@nestjs/axios';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigType } from '@nestjs/config';

import config from '../config';

const API_KEY = '123456';
const API_KEY_PRODUCTION = 'PRODUCTION_KEY_123456';

@Global()
@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      inject: [config.KEY],
      useFactory: (configService: ConfigType<typeof config>) => {
        const { user, host, password, port, dbName } = configService.mysql;
        return {
          type: 'mysql',
          host,
          port,
          username: user,
          password,
          database: dbName,
          // synchronize: true,
          synchronize: false,
          autoLoadEntities: true,
        };
      },
    }),
    HttpModule,
  ],
  providers: [
    {
      provide: 'API_KEY',
      useValue: process.env.NODE_ENV === 'prod' ? API_KEY_PRODUCTION : API_KEY,
    },
    {
      provide: 'TASKS_EXAMPLE',
      useFactory: async (http: HttpService) => {
        const request = http.get('https://jsonplaceholder.typicode.com/todos');
        const tasks = await lastValueFrom(request);
        return tasks.data;
      },
      inject: [HttpService],
    },
  ],
  exports: [TypeOrmModule, 'API_KEY', 'TASKS_EXAMPLE'],
})
export class DatabaseModule {}
