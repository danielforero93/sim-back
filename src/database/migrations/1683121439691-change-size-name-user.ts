import { MigrationInterface, QueryRunner } from "typeorm";

export class ChangeSizeNameUser1683121439691 implements MigrationInterface {
    name = 'ChangeSizeNameUser1683121439691'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`activity_module\` DROP FOREIGN KEY \`FK_0f6f85b2906fb5ab949c5d90935\``);
        await queryRunner.query(`ALTER TABLE \`activity_module\` CHANGE \`description\` \`description\` text NULL`);
        await queryRunner.query(`ALTER TABLE \`activity_module\` CHANGE \`createAt\` \`createAt\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP`);
        await queryRunner.query(`ALTER TABLE \`activity_module\` CHANGE \`updateAt\` \`updateAt\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(6)`);
        await queryRunner.query(`ALTER TABLE \`activity_module\` CHANGE \`moduleSimId\` \`moduleSimId\` int NULL`);
        await queryRunner.query(`DROP INDEX \`IDX_bd03a1ad0cc5066b1a50986cee\` ON \`module_sim\``);
        await queryRunner.query(`ALTER TABLE \`module_sim\` DROP COLUMN \`name\``);
        await queryRunner.query(`ALTER TABLE \`module_sim\` ADD \`name\` varchar(100) NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`module_sim\` ADD UNIQUE INDEX \`IDX_bd03a1ad0cc5066b1a50986cee\` (\`name\`)`);
        await queryRunner.query(`ALTER TABLE \`module_sim\` CHANGE \`createAt\` \`createAt\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP`);
        await queryRunner.query(`ALTER TABLE \`module_sim\` CHANGE \`updateAt\` \`updateAt\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(6)`);
        await queryRunner.query(`ALTER TABLE \`user\` DROP FOREIGN KEY \`FK_6aac19005cea8e2119cbe7759e8\``);
        await queryRunner.query(`ALTER TABLE \`user\` CHANGE \`createAt\` \`createAt\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP`);
        await queryRunner.query(`ALTER TABLE \`user\` CHANGE \`updateAt\` \`updateAt\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(6)`);
        await queryRunner.query(`ALTER TABLE \`user\` CHANGE \`personId\` \`personId\` int NULL`);
        await queryRunner.query(`ALTER TABLE \`person\` CHANGE \`createAt\` \`createAt\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP`);
        await queryRunner.query(`ALTER TABLE \`person\` CHANGE \`updateAt\` \`updateAt\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(6)`);
        await queryRunner.query(`ALTER TABLE \`activity_module\` ADD CONSTRAINT \`FK_0f6f85b2906fb5ab949c5d90935\` FOREIGN KEY (\`moduleSimId\`) REFERENCES \`module_sim\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`user\` ADD CONSTRAINT \`FK_6aac19005cea8e2119cbe7759e8\` FOREIGN KEY (\`personId\`) REFERENCES \`person\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`user\` DROP FOREIGN KEY \`FK_6aac19005cea8e2119cbe7759e8\``);
        await queryRunner.query(`ALTER TABLE \`activity_module\` DROP FOREIGN KEY \`FK_0f6f85b2906fb5ab949c5d90935\``);
        await queryRunner.query(`ALTER TABLE \`person\` CHANGE \`updateAt\` \`updateAt\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)`);
        await queryRunner.query(`ALTER TABLE \`person\` CHANGE \`createAt\` \`createAt\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)`);
        await queryRunner.query(`ALTER TABLE \`user\` CHANGE \`personId\` \`personId\` int NULL DEFAULT 'NULL'`);
        await queryRunner.query(`ALTER TABLE \`user\` CHANGE \`updateAt\` \`updateAt\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)`);
        await queryRunner.query(`ALTER TABLE \`user\` CHANGE \`createAt\` \`createAt\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)`);
        await queryRunner.query(`ALTER TABLE \`user\` ADD CONSTRAINT \`FK_6aac19005cea8e2119cbe7759e8\` FOREIGN KEY (\`personId\`) REFERENCES \`person\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`module_sim\` CHANGE \`updateAt\` \`updateAt\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)`);
        await queryRunner.query(`ALTER TABLE \`module_sim\` CHANGE \`createAt\` \`createAt\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)`);
        await queryRunner.query(`ALTER TABLE \`module_sim\` DROP INDEX \`IDX_bd03a1ad0cc5066b1a50986cee\``);
        await queryRunner.query(`ALTER TABLE \`module_sim\` DROP COLUMN \`name\``);
        await queryRunner.query(`ALTER TABLE \`module_sim\` ADD \`name\` varchar(255) NOT NULL`);
        await queryRunner.query(`CREATE UNIQUE INDEX \`IDX_bd03a1ad0cc5066b1a50986cee\` ON \`module_sim\` (\`name\`)`);
        await queryRunner.query(`ALTER TABLE \`activity_module\` CHANGE \`moduleSimId\` \`moduleSimId\` int NULL DEFAULT 'NULL'`);
        await queryRunner.query(`ALTER TABLE \`activity_module\` CHANGE \`updateAt\` \`updateAt\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)`);
        await queryRunner.query(`ALTER TABLE \`activity_module\` CHANGE \`createAt\` \`createAt\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)`);
        await queryRunner.query(`ALTER TABLE \`activity_module\` CHANGE \`description\` \`description\` text NULL DEFAULT 'NULL'`);
        await queryRunner.query(`ALTER TABLE \`activity_module\` ADD CONSTRAINT \`FK_0f6f85b2906fb5ab949c5d90935\` FOREIGN KEY (\`moduleSimId\`) REFERENCES \`module_sim\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
