// import * as dotenv from 'dotenv';
import { DataSource, DataSourceOptions } from 'typeorm';

// dotenv.config({path: })
export const AppDataSource = new DataSource({
  type: 'mysql',
  host: 'localhost',
  port: 3306,
  username: 'user',
  password: '123456',
  database: 'my_db',
  logging: true,
  synchronize: false,
  entities: ['src/**/**/*.entity.ts'],
  migrations: ['src/database/migrations/*.ts'],
  migrationsTableName: 'migrations',
});
