import { Controller, Get, UseGuards, SetMetadata } from '@nestjs/common';
import { AppService } from './app.service';
import { ApiTags } from '@nestjs/swagger';

import { ApiKeyGuard } from './auth/guards/api-key.guard';
import { Public } from './auth/decorators/public.decorator';

@UseGuards(ApiKeyGuard)
@ApiTags('ejemplo')
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Public()
  @Get('prueba')
  getPrueba(): string {
    return 'Hola mundo con Nest js';
  }

  @SetMetadata('isPublic', true)
  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  // @UseGuards(ApiKeyGuard)
  @Get('getApiKey')
  getApiKey(): string {
    return this.appService.getApiKey();
  }
}
