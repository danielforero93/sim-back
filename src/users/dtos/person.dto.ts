import { PartialType, ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CreatePersonDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: 'El nombre de la persona' })
  readonly name: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: 'El nombre de la persona' })
  readonly lastName: string;

  @IsNumber()
  @IsNotEmpty()
  @ApiProperty({ description: 'El número de identification de la persona' })
  readonly identification: string;
}

export class UpdatePersonDto extends PartialType(CreatePersonDto) {}
