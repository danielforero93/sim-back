import { PartialType, ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsPositive,
  IsString,
  Length,
} from 'class-validator';

export class CreateUserDto {
  @IsString()
  @IsEmail()
  @ApiProperty({ description: 'El email del usuario' })
  readonly email: string;

  // @IsStrongPassword()
  @IsString()
  @IsNotEmpty()
  @Length(6)
  @ApiProperty({ description: 'La contraseña del usuario' })
  readonly password: string;

  @IsNotEmpty()
  @ApiProperty({ description: 'El rol del usuairo' })
  readonly role: string;

  @IsOptional()
  @IsPositive()
  @ApiProperty()
  readonly personId: number;
}

export class UpdateUserDto extends PartialType(CreateUserDto) {}
