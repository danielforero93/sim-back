import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { CreateUserDto, UpdateUserDto } from 'src/users/dtos/user.dto';
import { User } from 'src/users/entities/user.entity';
import { PersonsService } from '../persons/persons.service';

@Injectable()
export class UsersService {
  constructor(
    private personsService: PersonsService,
    @InjectRepository(User) private userRepo: Repository<User>,
  ) {}

  findAll() {
    return this.userRepo.find({
      relations: ['person'],
    });
  }

  async findOne(id: number) {
    const user = await this.userRepo.findOne({ where: { id: id } });
    console.log(user);
    if (!user) throw new NotFoundException(`User # ${id} not found`);
    return user;
  }

  findByEmail(email: string) {
    return this.userRepo.findOne({ where: { email: email } });
  }

  async create(data: CreateUserDto) {
    const newUser = this.userRepo.create(data);
    const hashPassword = await bcrypt.hash(newUser.password, 10);
    newUser.password = hashPassword;
    if (data.personId) {
      const person = await this.personsService.findOne(data.personId);
      newUser.person = person;
    }
    return this.userRepo.save(newUser);
  }

  async update(id: number, changes: UpdateUserDto) {
    const user = await this.userRepo.findOneBy({ id });
    this.userRepo.merge(user, changes);
    return this.userRepo.save(user);
  }

  remove(id: number) {
    return this.userRepo.delete(id);
  }
}
