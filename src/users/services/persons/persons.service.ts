import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Person } from 'src/users/entities/person.entity';
import { CreatePersonDto, UpdatePersonDto } from 'src/users/dtos/person.dto';

@Injectable()
export class PersonsService {
  constructor(
    @InjectRepository(Person) private personRepo: Repository<Person>,
  ) {}

  findAll() {
    return this.personRepo.find();
  }

  async findOne(id: number) {
    const person = await this.personRepo.findOne({ where: { id: id } });
    console.log(person);
    if (!person) throw new NotFoundException(`Person # ${id} not found`);
    return person;
  }

  create(data: CreatePersonDto) {
    const newPerson = this.personRepo.create(data);
    return this.personRepo.save(newPerson);
  }

  async update(id: number, changes: UpdatePersonDto) {
    const user = await this.personRepo.findOneBy({ id });
    this.personRepo.merge(user, changes);
    return this.personRepo.save(user);
  }

  remove(id: number) {
    return this.personRepo.delete(id);
  }
}
