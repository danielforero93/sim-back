import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
} from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { CreatePersonDto, UpdatePersonDto } from 'src/users/dtos/person.dto';
import { PersonsService } from 'src/users/services/persons/persons.service';

@ApiTags('persons')
@Controller('persons')
export class PersonsController {
  constructor(private personsService: PersonsService) {}

  @Get(':id')
  get(@Param('id', ParseIntPipe) id: number) {
    return this.personsService.findOne(id);
  }

  @ApiOperation({ summary: 'Lista de personas' })
  @Get()
  getAll() {
    return this.personsService.findAll();
  }

  @Post()
  create(@Body() payload: CreatePersonDto) {
    return this.personsService.create(payload);
  }

  @Put(':id')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() payload: UpdatePersonDto,
  ) {
    return this.personsService.update(id, payload);
  }

  @Delete(':id')
  remove(@Param('id', ParseIntPipe) id: number) {
    return this.personsService.remove(id);
  }
}
