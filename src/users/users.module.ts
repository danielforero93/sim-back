import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { UsersController } from './controllers/users/users.controller';
import { UsersService } from './services/users/users.service';
import { User } from './entities/user.entity';
import { Person } from './entities/person.entity';
import { PersonsService } from './services/persons/persons.service';
import { PersonsController } from './controllers/persons/persons.controller';

@Module({
  imports: [TypeOrmModule.forFeature([User, Person])],
  controllers: [UsersController, PersonsController],
  providers: [UsersService, PersonsService],
  exports: [UsersService],
})
export class UsersModule {}
